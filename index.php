<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require 'vendor/autoload.php';

    use GeoIp2\Database\Reader;

    function getIp()
    {
        $keys = [
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR'
        ];
        foreach ($keys as $key) {
            if (!empty($_SERVER[$key])) {
                $ip = trim(end(explode(',', $_SERVER[$key])));
                if (filter_var($ip, FILTER_VALIDATE_IP)) {
                    return $ip;
                }
            }
        }
    }

    // $ip = getIp();
    // echo 'ip = ' . $ip;
    $ip = '185.108.162.21'; //мой ip для проверки работоспособности прогноза погоды

    $reader = new Reader('GeoLite2-City.mmdb');
    $record = $reader->city($ip);

    const API = '00592c7761d3c0a489a9ff14abcfabfd'; //ключ полученный мной в личном кабинете  OpenWeatherMap

    $cityName = $record->city->name;
    $url = "http://api.openweathermap.org/data/2.5/weather?q=$cityName&appid=" . API . "&units=metric&lang=ru";
    $content = file_get_contents($url);
    $result = json_decode($content, true);
    print_r($result);
    ?>
</body>

</html>